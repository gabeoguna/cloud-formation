#!/bin/bash

aws cloudformation create-stack \
  --region us-east-1 \
  --stack-name myVPC \
  --template-body file://create-vpc-template.json \
