#!/bin/bash

aws cloudformation create-stack \
  --stack-name bucket2 \
  --template-body file://create-bucket2-template.json \
  --parameters \
    ParameterKey=BucketName,ParameterValue=myuniquebucketnamegoesrighthere \
    ParameterKey=Environment,ParameterValue=dev \
