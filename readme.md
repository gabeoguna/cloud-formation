# AWS Cloud Formation
A helper project that shows how to use Cloud Formation.

Cloud Formation has [limits](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cloudformation-limits.html), which doesn't make it very cloudy.  :-)

# Getting Started
- Execute `aws configure` to configure your AWS keys.

#### Most Basic Example: Creating a Bucket
```
aws cloudformation create-stack --stack-name bucket --template-body file://create-bucket-template.json
aws cloudformation describe-stacks --stack-name bucket
aws cloudformation delete-stack --stack-name bucket
```

#### S3: Pass Multiple Inputs to Create a Bucket
```
./create-bucket2.sh
aws cloudformation describe-stacks --stack-name bucket2
aws cloudformation delete-stack --stack-name bucket2
```

#### Creating a VPC
```
./create-vpc.sh
aws cloudformation describe-stacks --stack-name myVPC
aws cloudformation delete-stack --stack-name myVPC
```
Fun fact: Cloud Formation cannot get a reference to the main route table.

# Handy URLs
Template Snippets:
- https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/CHAP_TemplateQuickRef.html

Resource Types:
- https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-template-resource-type-ref.html

CLI Reference: 
- https://docs.aws.amazon.com/cli/latest/reference/cloudformation/index.html#cli-aws-cloudformation
- https://docs.aws.amazon.com/cli/latest/reference/cloudformation/create-stack.html

Intrinisic Functions:
- https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/intrinsic-function-reference.html


# Cloud Formation File Format
- This describes the basic structure of a JSON Cloud Formation template file:

```
{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "",
  "Parameters" : {}, // Max: 60 inputs
  "Resources" : {},  // Max: 200 resources
  "Outputs": {},     // Max: 60 outputs
  "Mappings" : {}
}
```

# Fn::Join
- Returns a string
- Takes two arguments: i) The string used to join the other strings, ii) An array of strings to join.
- Example: `"Fn::Join": [ "-", [{ "Ref": "Environment" }, { "Ref": "BucketName" } ]]`
- Returns: `dev-myBucket`

# Fn::GetAtt
- Returns the value of an attribute (property) from a resource in the template.
- Takes two arguments: i) The name you specified for a resource, ii) The attribute (property) you want the value for.
- Example: `"Fn::GetAtt" : [ "myELB" , "DNSName" ]`

# PRO Tip
- Parameter Key/Values cannot have a space after the comma when using the aws cloudformation cli:
- Example: `--parameters ParameterKey=BucketName, [NO SPACE ALLOWED HERE!] ParameterValue=mysuperuniquebucketnamerighthere`
